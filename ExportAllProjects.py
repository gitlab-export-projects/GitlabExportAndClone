import json
import requests

# This script is to be run before the DownloadAllProjects.py

#declare - initialise variables
pToken="XXX"
header = {"PRIVATE-TOKEN": pToken}
count = 0

response = requests.get('https://gitlab.com/api/v4/groups/<groupOrgName>/projects?include_subgroups=true&per_page=999&page=1', headers = header)

data = response.json()
items = data

#Schedule All projects for exports visible by the user running this script
for item in items:
    count +=1
    itemId = item['id']
    url = 'https://gitlab.com/api/v4/projects/' + str(itemId) + '/export'
    response = requests.post(url, headers = header)
    print count, response.text

#End of Python Script
