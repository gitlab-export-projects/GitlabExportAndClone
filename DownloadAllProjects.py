import json
import requests
import os

#This is to be run after ExportAllProjects.py      
      
#declare - initialise variables
pToken="XXX"
header = {"PRIVATE-TOKEN": pToken}
count = 0

response = requests.get('https://gitlab.com/api/v4/groups/<OrgGroupName>/projects?include_subgroups=true&per_page=999&page=1', headers = header)

data = response.json()
items = data

#Download All projects (that have been exported) by the user running this script
for item in items:
    count +=1
    itemId = item['id']
    n1 = item['namespace']['full_path']
    n2 = n1+'/'+item['name']+'.tgz'
    url = 'https://gitlab.com/api/v4/projects/' + str(itemId) + '/export/download'
    response = requests.get(url, headers=header)
    #print (n1+'/'+n2)
    os.makedirs(n1, exist_ok=True)
    fo = open(n2, "wb")
    fo.write(response.content)
    fo.close


#End of Python Script

