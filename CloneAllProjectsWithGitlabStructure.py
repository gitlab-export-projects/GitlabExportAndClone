import json
import requests
import os

#to be run post gitlab ssh configuration      
      
#declare - initialise variables
pToken="xxxx"
header = {"PRIVATE-TOKEN": pToken}
count = 0

response = requests.get('https://gitlab.com/api/v4/groups/<Id:OfGroup>/projects?include_subgroups=true&per_page=999&page=1', headers = header)

data = response.json()
items = data

#Download All projects (that have been exported) by the user running this script
for item in items:
    count +=1
    itemId = item['id']
    n1 = item['namespace']['full_path']
    origin = item ['ssh_url_to_repo']
    n2 = n1+'/'+item['name']+'.tgz'

    cwd = os.getcwd()
    os.makedirs(n1, exist_ok=True)
    os.chdir(n1)
    os.system ('git clone '+origin)
    os.chdir(cwd)
    n1=""
    origin=""
    n2=""
    itemId=""
   
    
#End of Python Script
